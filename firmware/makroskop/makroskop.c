/*
 * Firmware k makroskopu
 *
 * Created: 8/13/2014 10:50:50 AM
 *  Author: Dalibor Šulc
 */ 

#define F_CPU  16000000
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdint.h>

#define LEDON PORTC |= 1<<PINC1;
#define LEDOFF PORTC &= ~(1<<PINC1); 
#define LEDTOGGLE PORTC ^= (1<<PINC1); 

#define MOT_A_ON PORTD |= (1<<PINB6);
#define MOT_A_OFF PORTD &= ~(1<<PIND6);

#define MOT_A_LEFT PORTC |= (1<<PINC4);
#define MOT_A_RIGHT PORTC &= ~(1<<PINC4);

#define MOT_B_LEFT PORTC |= (1<<PINC5);
#define MOT_B_RIGHT PORTC &= ~(1<<PINC5);


#define MOT_B_ON PORTD |= (1<<PINB7);
#define MOT_B_OFF PORTD &= ~(1<<PIND7);

#define MAX_STRIDA 255

#define VERSION "0.2"

//#################### definice typù ####################
typedef unsigned char u8;
typedef signed char s8;
typedef signed short s16;
typedef unsigned short u16;
typedef signed long s32;
typedef unsigned long u32;

//#################### globalni promenne ####################

volatile s32 pos_A = 0;
volatile s32 pos_B = 0;
volatile s32 pos_A_pozadovana = 0;
volatile s32 pos_B_pozadovana = 0;
volatile s32 sum_odchA=0;
volatile s32 sum_odchB=0;
volatile u8 odesli_zpravu = 0; 
volatile s32 debug1 = 0;
volatile s32 debug2 = 0;
volatile s32 debug3 = 0;

volatile s32 pos_A_prev = 0;
volatile s32 pos_B_prev = 0;
volatile s32 vel_pozadovana = 0x01;
u8  received[13]       = {0,0,0,0,0,0,0,0,0,0,0,0,0}; // prijatá data z pc
u8  cb                = 0;
s16 limiter_A = MAX_STRIDA;
s16 limiter_B = MAX_STRIDA;

volatile u16 hal_adc = 0;
volatile u16 iA_adc = 0;
volatile u16 iB_adc = 0;
volatile u8 adc_switch = 0;

//#################### program ####################
int init(void)
{
	// inicializace portu a registru
	DDRB = (1<<PINB1)|(1<<PINB2);
	
	DDRC = (1<<PINC0)|(1<<PINC1)|(1<<PINC2)|(1<<PINC4)|(1<<PINC5);
	
	DDRD = (1<<PIND1)|(1<<PIND6)|(1<<PIND7);
	PORTD = (1<<PIND0);
	
	// USART
	// init USARTu
	//UCSR0A = 1<<U2X0; // hrubsi vzorkovani
	UCSR0C = (1<<UCSZ00)|(1<<UCSZ01); // asynchronni, bez parity,
	// 1 stop bit, 8 datovych bitu
	UBRR0H = 0;
	UBRR0L = 12;
	UCSR0B = (1<<RXEN0)|(1<<TXEN0)|(1<<RXCIE0); // zapneme vysilac i prijimac a preruseni
	
	// halovka a proud
	ADMUX = 3; // ADC3
	ADCSRA = (1<<ADEN)|(1<<ADIE)|(1<<ADPS2)|(1<<ADPS1);
	
	// preruseni od enkoderu
	EICRA = (1<<ISC11)|(1<<ISC01);
	EIMSK = (1<<INT1)|(1<<INT0);
	
	// PWM motoru
	TCCR1A = (1<<COM1A1)|(1<<COM1B1);
	TCCR1B = (1<<CS10)|(1<<WGM13);
	ICR1 = MAX_STRIDA;
	
	// spousteni regulatoru
	TCCR0B = (1<<CS01)|(1<<CS00); 
	TIMSK0 = (1<<TOIE0);
	
	// spousteni omezovace
	TCCR2B = (1<<CS22)|(1<<CS20);
	TIMSK2 = (1<<TOIE2);
	
	return 1;
}

void motA(s32 strida)
{
	// saturace
	if (strida > limiter_A)
		strida = limiter_A;
	else if (strida< (-limiter_A))
		strida = -limiter_A;
	
	// hlidani zmeny smeru
	if (strida<0)
	{
		MOT_A_RIGHT
	}
	else
	{
		MOT_A_LEFT
	}
	//puts16(strida);
	//putstring("\n\r");
	// posleme spravnou stridu
	if (strida < 0)
		OCR1A = (u16)(-strida);
	else if (strida >= 0)
		OCR1A = (u16)(MAX_STRIDA-strida);
	
	//debug3 = OCR1A;
}

void motB(s32 strida)
{
	// saturace
	if (strida > limiter_B)
	strida = limiter_B;
	else if (strida< (-limiter_B))
	strida = -limiter_B;
	
	// hlidani zmeny smeru
	if (strida<0)
	{
		MOT_B_RIGHT
	}
	else
	{
		MOT_B_LEFT
	}
	//puts16(strida);
	//putstring("\n\r");
	// posleme spravnou stridu
	if (strida < 0)
	OCR1B = (u16)(-strida);
	else if (strida >= 0)
	OCR1B = (u16)(MAX_STRIDA-strida);
}

int putchar(int c)
{
	while(!(UCSR0A & (1<<UDRE0))); // pockej na dokonceni predchazejiciho vysilani
	return UDR0 = c; // vysli znak
}

void putu16(u16 i)
{
	// desetitisice
	u8 n = '0';
	while(i >= 10000)
	n++,i-=10000;
	putchar(n);
	// tisice
	n = '0';
	while(i >= 1000)
	n++,i-=1000;
	putchar(n);
	// stovky
	n = '0';
	while(i >= 100)
	n++,i-=100;
	putchar(n);
	// desitky a jednotky
	n = '0';
	u8 iz = i; // i je ted zarucene mensi jak 100
	while(iz >= 10)
	n++,iz-=10;
	putchar(n);
	putchar(iz+'0');
}

void puts16(s16 i)
{
	if(i<0)
	{
		putchar('-');
		putu16(-i);
	}
	else
	{
		putchar('+');
		putu16(i);
	}
}

void putu32(u32 i)
{
	// miliardy
	u8 n = '0';
	while(i >= 1000000000)
	n++,i-=1000000000;
	putchar(n);
	putchar(',');
	// stovky milionù
	n = '0';
	while(i >= 100000000)
	n++,i-=100000000;
	putchar(n);
	// desítky milionù
	n = '0';
	while(i >= 10000000)
	n++,i-=10000000;
	putchar(n);
	// miliony
	n = '0';
	while(i >= 1000000)
	n++,i-=1000000;
	putchar(n);
	putchar(',');
	// statisíce
	n = '0';
	while(i >= 100000)
	n++,i-=100000;
	putchar(n);
	// desetitisice
	n = '0';
	while(i >= 10000)
	n++,i-=10000;
	putchar(n);
	// tisice
	n = '0';
	while(i >= 1000)
	n++,i-=1000;
	putchar(n);
	putchar(',');
	// stovky
	n = '0';
	while(i >= 100)
	n++,i-=100;
	putchar(n);
	// desitky a jednotky
	n = '0';
	u8 iz = i; // i je ted zarucene mensi jak 100
	while(iz >= 10)
	n++,iz-=10;
	putchar(n);
	putchar(iz+'0');
}

void puts32(s32 i)
{
	if(i<0)
	{
		putchar('-');
		putu32(-i);
	}
	else
	{
		putchar('+');
		putu32(i);
	}
}

void putstring(u8 *p)
{
	u8 *copy_of_p = p;
	u8 c;
	while(c = *copy_of_p++)
		putchar(c);
}

void putHex(s32 dec,u8 offset)
{
	for (int i=(offset-1);i>=0;i--)
	{
		u8 prvek = (dec >> (i*4)) & 0x0F;
		if (prvek <10)
			putchar('0'+prvek);
		else
			putchar('a'+(prvek - 10));
	}
}

void sendMessage ()
{
  /*putu32(pos_A);
  putchar('\n');
  putu32(pos_B);
  putchar('\n');*/
  // ve tvaru P  F FF FF   F FF FF  0FFF
  //           |    Y    |    X   |  ADC
  // start znak 
  putchar('P');
  // Y pozice
  putHex(pos_A + 0x7FFFF,5);
  // Z pozice
  putHex(pos_B + 0x7FFFF,5);
  // ADC prevod
  putHex(hal_adc,3);
  putHex(iA_adc,3);
  putHex(iB_adc,3);
  
  odesli_zpravu = 0;
}

u8 hex2dec(u8 hex)
{
	if (hex<='9')
		return (hex-'0');
	else
		return ((hex-'a')+10);
}

u32 strHexToInt(u8 offset, u8 num)
{
	s32 vysledek = 0;
	for (int i=(num-1);i>=0;i--)
	{
		vysledek += ((s32)hex2dec(received[i+offset]))<<(4*((num-1)-i));
	}
	//putu32(vysledek);
	//putchar('\t');
	return vysledek;
}

void zpracuj_prijem(void)
{
  // zapneme konverzi AD prevodniku
  ADCSRA |=(1<<ADSC);
  
  // predame pozadovane hodnoty
  pos_A_pozadovana = strHexToInt(0,5) - 0x7FFFF;
  pos_B_pozadovana = strHexToInt(5,5) - 0x7FFFF;
  vel_pozadovana = strHexToInt(10,2);
  
  odesli_zpravu = 1;
 /*putstring("ZADANA POLOHA:");
  putchar(10);putchar(13);
  putu32(pos_x_zadana);
  putchar(10);putchar(13);
  putu32(pos_y_zadana);
  putchar(10);putchar(13);
  putstring("SKUTECNA POLOHA:");
  putchar(10);putchar(13);
  putu32(pos_x);
  putchar(10);putchar(13);
  putu32(pos_y);
  putchar(10);putchar(13);*/
}

ISR (USART_RX_vect)
{
	u8 prijem = UDR0;
	if (prijem == 'H')
		putchar('O');
	else if (prijem == 'S')
		odesli_zpravu = 1;
	else
	{
		// regulary communication
		if (prijem=='M')
		{
			for (int i=12;i>=0;i--)
				received[i] = 0;
			cb = 0;
		}
		// naplnìní pole daty
		else
			received[cb++] = prijem;
		// pokud jsou data všechna - zpracujeme
		if (cb==13)
		{
			u8 xor = 0;
			for (u8 i=0;i<(cb-1);i++)
			{
				xor ^= received[i];
			}

			if (received[cb-1] == 'x')//xor)
			{
				//LEDTOGGLE
				zpracuj_prijem();
			}
		}
		if (cb>13)
			cb=13;
	}
		
}

ISR (ADC_vect)
{
	sei();
	if (adc_switch == 0)
	{
		hal_adc = ADC;
		ADMUX = 6;	
		adc_switch++;
	}
	else if (adc_switch == 1)
	{
		iA_adc = ADC;
		ADMUX = 7;
		adc_switch++;
	}
	else
	{
		iB_adc = ADC;
		ADMUX = 3;
		adc_switch = 0;
	}
	
	ADCSRA |= (1<<ADSC); 
}

ISR (INT0_vect)
{
	if (PIND & (1<<PIND4)) // podle bitu 4 bud zvetsi nebo zmensi polohu
	{
		pos_A--;
		//putchar('-');
	}
	else
	{
		pos_A++;
		//putchar('+');
	}
}

ISR (INT1_vect)
{
	if (PIND & (1<<PIND5)) // podle bitu 4 bud zvetsi nebo zmensi polohu
	{
		pos_B--;
	}
	else
	{
		pos_B++;
	}	
}

ISR (TIMER0_OVF_vect) // regulator
{
	sei();
	s32 odchylka = pos_A_pozadovana - pos_A;
	sum_odchA += odchylka;
	debug1 = odchylka;
	if (sum_odchA>1320)
	{	
		sum_odchA = 1320;
	}
	if (sum_odchA < -1320)
	{
		sum_odchA = -1320;
	}
	debug2 = sum_odchA;
	s32 strida = (odchylka) + (sum_odchA>>4);
	debug3 = strida;
	motA(strida);
	
	s32 odchylkaB = pos_B_pozadovana - pos_B;
	sum_odchB += odchylkaB;
	if (sum_odchB>1320)
	{
		sum_odchB = 1320;
	}
	if (sum_odchB < -1320)
	{
		sum_odchB = -1320;
	}

	strida = (odchylkaB) + (sum_odchB>>4);
	motB(strida);
}

ISR (TIMER2_OVF_vect)
{
	sei();
	s32 vel_A = pos_A - pos_A_prev;
	s32 vel_B = pos_B - pos_B_prev;
	//puts16(vel_A);
	//putchar('-');
	s32 odchylka_A = abs(vel_A) - vel_pozadovana;
	s32 odchylka_B = abs(vel_B) - vel_pozadovana;
	if (odchylka_A > 0)
	{
		s32 zasah = odchylka_A<<4;// + odchylka>>2;
		//debug2 = zasah;
		//puts16(zasah);	
		//putchar('\t');	
	
		if (zasah > MAX_STRIDA)
			zasah = MAX_STRIDA; 
		limiter_A = MAX_STRIDA - (zasah);
		
	}
	else 
		limiter_A = MAX_STRIDA;
	
	if (odchylka_B > 0)
		{
			s32 zasah = odchylka_B<<4;// + odchylka>>2;
			//debug2 = zasah;
			//puts16(zasah);
			//putchar('\t');
			
			if (zasah > MAX_STRIDA)
			zasah = MAX_STRIDA;
			limiter_B = MAX_STRIDA - (zasah);
			
		}
		else
			limiter_B = MAX_STRIDA;
			
	//debug1 = limiter;
	//putchar('\n');
	pos_A_prev = pos_A;	
	pos_B_prev = pos_B;	
}

int main(void)
{
	init();
	sei();
	
	ADCSRA |= (1<<ADSC); 
	
	//OCR1A = 50;
	MOT_A_ON
	MOT_B_ON
	//pos_A_pozadovana = 10000;
    while(1)
    {
		if (odesli_zpravu)
		{
			sendMessage();
			//putchar(10);putchar(13);
		}
		
		// current limiter
		if (iA_adc > 200 || iB_adc > 200)
		{
			MOT_A_OFF
			MOT_B_OFF
			LEDON
		}
		
		//LEDON
		//putchar(64);
		//ADCSRA |= (1<<ADSC); 
		//_delay_ms(1000);
		//LEDOFF
		//putu32(pos_A);
		//putchar(10);
		/*
		puts32(debug1);
		putchar('\t');
		puts32(debug2);
		putchar('\t');	
		puts32(debug3);
		putchar('\n');*/
		
		/*puts32(hal_adc);
		putchar('\t');
		puts32(iA_adc);
		putchar('\t');
		puts32(iB_adc);
		putchar('\n');*/
		
		_delay_ms(1);		 
		
		//motA(-20);
		//_delay_ms(1000);
		
    }
	
	return 1;
}

