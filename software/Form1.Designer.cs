﻿namespace Makroskop
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.main_left_panel = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.xSize_textBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.scan_button = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.lineTool_radioButton = new System.Windows.Forms.RadioButton();
            this.button1 = new System.Windows.Forms.Button();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.pocetPxX_textBox = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.velocity_textBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.opakovat_checkBox = new System.Windows.Forms.CheckBox();
            this.return_checkBox = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.mainB_pictureBox = new System.Windows.Forms.PictureBox();
            this.main_pictureBox = new System.Windows.Forms.PictureBox();
            this.scale_pictureBox = new System.Windows.Forms.PictureBox();
            this.zedGraphControl1 = new ZedGraph.ZedGraphControl();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.moveTop_button = new System.Windows.Forms.Button();
            this.moveRight_button = new System.Windows.Forms.Button();
            this.moveDown_button = new System.Windows.Forms.Button();
            this.moveLeft_button = new System.Windows.Forms.Button();
            this.calibrate_button = new System.Windows.Forms.Button();
            this.logo_button = new System.Windows.Forms.Button();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.serial_StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.errors_toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.Scanning_backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.redraw_timer = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainB_pictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.main_pictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scale_pictureBox)).BeginInit();
            this.tableLayoutPanel10.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // main_left_panel
            // 
            this.main_left_panel.AutoSize = true;
            this.main_left_panel.Dock = System.Windows.Forms.DockStyle.Left;
            this.main_left_panel.Location = new System.Drawing.Point(0, 0);
            this.main_left_panel.Name = "main_left_panel";
            this.main_left_panel.Size = new System.Drawing.Size(0, 612);
            this.main_left_panel.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel7, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.zedGraphControl1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel10, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.logo_button, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 65F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 171F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(852, 581);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Image = global::Makroskop.Properties.Resources.makroskop_logo2;
            this.label1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label1.Location = new System.Drawing.Point(10, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.label1.MaximumSize = new System.Drawing.Size(595, 65);
            this.label1.MinimumSize = new System.Drawing.Size(595, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(595, 65);
            this.label1.TabIndex = 1;
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.scan_button, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.label11, 0, 8);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel8, 0, 9);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel5, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel11, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel6, 0, 4);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(655, 68);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 11;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(194, 339);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(188, 30);
            this.label4.TabIndex = 0;
            this.label4.Text = "Nastavení skenování:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel3.Controls.Add(this.xSize_textBox, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label3, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 33);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(188, 24);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // xSize_textBox
            // 
            this.xSize_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xSize_textBox.Location = new System.Drawing.Point(83, 3);
            this.xSize_textBox.Name = "xSize_textBox";
            this.xSize_textBox.Size = new System.Drawing.Size(64, 20);
            this.xSize_textBox.TabIndex = 0;
            this.xSize_textBox.Text = "100";
            this.xSize_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(150, 5);
            this.label3.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 19);
            this.label3.TabIndex = 2;
            this.label3.Text = "mm";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 24);
            this.label2.TabIndex = 0;
            this.label2.Text = "Rozměr:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // scan_button
            // 
            this.scan_button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.scan_button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scan_button.Enabled = false;
            this.scan_button.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.scan_button.Location = new System.Drawing.Point(3, 198);
            this.scan_button.Name = "scan_button";
            this.scan_button.Size = new System.Drawing.Size(188, 44);
            this.scan_button.TabIndex = 5;
            this.scan_button.Text = "Skenovat!";
            this.scan_button.UseVisualStyleBackColor = false;
            this.scan_button.Click += new System.EventHandler(this.scan_button_Click);
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Verdana", 11.25F);
            this.label11.Location = new System.Drawing.Point(3, 275);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(188, 30);
            this.label11.TabIndex = 0;
            this.label11.Text = "Nástroje analýzy:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 4;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel8.Controls.Add(this.lineTool_radioButton, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.button1, 3, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(0, 305);
            this.tableLayoutPanel8.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(194, 30);
            this.tableLayoutPanel8.TabIndex = 7;
            // 
            // lineTool_radioButton
            // 
            this.lineTool_radioButton.AllowDrop = true;
            this.lineTool_radioButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.lineTool_radioButton.AutoSize = true;
            this.lineTool_radioButton.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lineTool_radioButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lineTool_radioButton.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lineTool_radioButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lineTool_radioButton.Location = new System.Drawing.Point(1, 1);
            this.lineTool_radioButton.Margin = new System.Windows.Forms.Padding(1);
            this.lineTool_radioButton.Name = "lineTool_radioButton";
            this.lineTool_radioButton.Size = new System.Drawing.Size(46, 28);
            this.lineTool_radioButton.TabIndex = 0;
            this.lineTool_radioButton.TabStop = true;
            this.lineTool_radioButton.Text = "profil";
            this.lineTool_radioButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lineTool_radioButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.lineTool_radioButton.UseVisualStyleBackColor = true;
            this.lineTool_radioButton.CheckedChanged += new System.EventHandler(this.lineTool_radioButton_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Location = new System.Drawing.Point(147, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(44, 24);
            this.button1.TabIndex = 1;
            this.button1.Text = "Export";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel5.ColumnCount = 3;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel5.Controls.Add(this.label8, 2, 0);
            this.tableLayoutPanel5.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.pocetPxX_textBox, 1, 0);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 63);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(188, 24);
            this.tableLayoutPanel5.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.Location = new System.Drawing.Point(150, 5);
            this.label8.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 19);
            this.label8.TabIndex = 2;
            this.label8.Text = "px";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 24);
            this.label5.TabIndex = 2;
            this.label5.Text = "Počet px:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pocetPxX_textBox
            // 
            this.pocetPxX_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pocetPxX_textBox.Location = new System.Drawing.Point(83, 3);
            this.pocetPxX_textBox.Name = "pocetPxX_textBox";
            this.pocetPxX_textBox.Size = new System.Drawing.Size(64, 20);
            this.pocetPxX_textBox.TabIndex = 0;
            this.pocetPxX_textBox.Text = "50";
            this.pocetPxX_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel11.ColumnCount = 3;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel11.Controls.Add(this.label7, 2, 0);
            this.tableLayoutPanel11.Controls.Add(this.velocity_textBox, 1, 0);
            this.tableLayoutPanel11.Controls.Add(this.label12, 0, 0);
            this.tableLayoutPanel11.Location = new System.Drawing.Point(3, 93);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 1;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(188, 24);
            this.tableLayoutPanel11.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(150, 5);
            this.label7.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 19);
            this.label7.TabIndex = 2;
            this.label7.Text = "enc/s";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // velocity_textBox
            // 
            this.velocity_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.velocity_textBox.Location = new System.Drawing.Point(83, 3);
            this.velocity_textBox.Name = "velocity_textBox";
            this.velocity_textBox.Size = new System.Drawing.Size(64, 20);
            this.velocity_textBox.TabIndex = 0;
            this.velocity_textBox.Text = "2";
            this.velocity_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label12.Location = new System.Drawing.Point(3, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(74, 24);
            this.label12.TabIndex = 2;
            this.label12.Text = "Rychlost:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Controls.Add(this.opakovat_checkBox, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.return_checkBox, 1, 0);
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 123);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(188, 54);
            this.tableLayoutPanel6.TabIndex = 3;
            // 
            // opakovat_checkBox
            // 
            this.opakovat_checkBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.opakovat_checkBox.AutoSize = true;
            this.opakovat_checkBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.opakovat_checkBox.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.opakovat_checkBox.Location = new System.Drawing.Point(3, 3);
            this.opakovat_checkBox.Name = "opakovat_checkBox";
            this.opakovat_checkBox.Size = new System.Drawing.Size(88, 48);
            this.opakovat_checkBox.TabIndex = 0;
            this.opakovat_checkBox.Text = "Opakovat měření";
            this.opakovat_checkBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.opakovat_checkBox.UseVisualStyleBackColor = true;
            // 
            // return_checkBox
            // 
            this.return_checkBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.return_checkBox.AutoSize = true;
            this.return_checkBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.return_checkBox.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.return_checkBox.Location = new System.Drawing.Point(97, 3);
            this.return_checkBox.Name = "return_checkBox";
            this.return_checkBox.Size = new System.Drawing.Size(88, 48);
            this.return_checkBox.TabIndex = 0;
            this.return_checkBox.Text = "Vracet hrot do vých. p.";
            this.return_checkBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.return_checkBox.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 3;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel7.Controls.Add(this.mainB_pictureBox, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.main_pictureBox, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.scale_pictureBox, 2, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 68);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(646, 339);
            this.tableLayoutPanel7.TabIndex = 3;
            // 
            // mainB_pictureBox
            // 
            this.mainB_pictureBox.BackColor = System.Drawing.Color.White;
            this.mainB_pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mainB_pictureBox.Cursor = System.Windows.Forms.Cursors.Cross;
            this.mainB_pictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainB_pictureBox.Location = new System.Drawing.Point(308, 3);
            this.mainB_pictureBox.Name = "mainB_pictureBox";
            this.mainB_pictureBox.Size = new System.Drawing.Size(299, 333);
            this.mainB_pictureBox.TabIndex = 2;
            this.mainB_pictureBox.TabStop = false;
            // 
            // main_pictureBox
            // 
            this.main_pictureBox.BackColor = System.Drawing.Color.White;
            this.main_pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.main_pictureBox.Cursor = System.Windows.Forms.Cursors.Cross;
            this.main_pictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_pictureBox.Location = new System.Drawing.Point(3, 3);
            this.main_pictureBox.Name = "main_pictureBox";
            this.main_pictureBox.Size = new System.Drawing.Size(299, 333);
            this.main_pictureBox.TabIndex = 0;
            this.main_pictureBox.TabStop = false;
            this.main_pictureBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.main_pictureBox_mouseMove);
            this.main_pictureBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.main_pictureBox_MouseClick);
            // 
            // scale_pictureBox
            // 
            this.scale_pictureBox.BackColor = System.Drawing.Color.White;
            this.scale_pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scale_pictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scale_pictureBox.Location = new System.Drawing.Point(613, 3);
            this.scale_pictureBox.Name = "scale_pictureBox";
            this.scale_pictureBox.Size = new System.Drawing.Size(30, 333);
            this.scale_pictureBox.TabIndex = 1;
            this.scale_pictureBox.TabStop = false;
            // 
            // zedGraphControl1
            // 
            this.zedGraphControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zedGraphControl1.Location = new System.Drawing.Point(3, 413);
            this.zedGraphControl1.Name = "zedGraphControl1";
            this.zedGraphControl1.ScrollGrace = 0;
            this.zedGraphControl1.ScrollMaxX = 0;
            this.zedGraphControl1.ScrollMaxY = 0;
            this.zedGraphControl1.ScrollMaxY2 = 0;
            this.zedGraphControl1.ScrollMinX = 0;
            this.zedGraphControl1.ScrollMinY = 0;
            this.zedGraphControl1.ScrollMinY2 = 0;
            this.zedGraphControl1.Size = new System.Drawing.Size(646, 165);
            this.zedGraphControl1.TabIndex = 4;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 3;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.Controls.Add(this.moveTop_button, 1, 0);
            this.tableLayoutPanel10.Controls.Add(this.moveRight_button, 2, 1);
            this.tableLayoutPanel10.Controls.Add(this.moveDown_button, 1, 2);
            this.tableLayoutPanel10.Controls.Add(this.moveLeft_button, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.calibrate_button, 1, 1);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(655, 413);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 3;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(194, 165);
            this.tableLayoutPanel10.TabIndex = 5;
            // 
            // moveTop_button
            // 
            this.moveTop_button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.moveTop_button.Enabled = false;
            this.moveTop_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.moveTop_button.Location = new System.Drawing.Point(67, 3);
            this.moveTop_button.Name = "moveTop_button";
            this.moveTop_button.Size = new System.Drawing.Size(58, 49);
            this.moveTop_button.TabIndex = 0;
            this.moveTop_button.Text = "↑";
            this.moveTop_button.UseVisualStyleBackColor = true;
            this.moveTop_button.Click += new System.EventHandler(this.moveTop_button_Click);
            // 
            // moveRight_button
            // 
            this.moveRight_button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.moveRight_button.Enabled = false;
            this.moveRight_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.moveRight_button.Location = new System.Drawing.Point(131, 58);
            this.moveRight_button.Name = "moveRight_button";
            this.moveRight_button.Size = new System.Drawing.Size(60, 49);
            this.moveRight_button.TabIndex = 0;
            this.moveRight_button.Text = "→";
            this.moveRight_button.UseVisualStyleBackColor = true;
            this.moveRight_button.Click += new System.EventHandler(this.moveRight_button_Click);
            // 
            // moveDown_button
            // 
            this.moveDown_button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.moveDown_button.Enabled = false;
            this.moveDown_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.moveDown_button.Location = new System.Drawing.Point(67, 113);
            this.moveDown_button.Name = "moveDown_button";
            this.moveDown_button.Size = new System.Drawing.Size(58, 49);
            this.moveDown_button.TabIndex = 0;
            this.moveDown_button.Text = "↓";
            this.moveDown_button.UseVisualStyleBackColor = true;
            this.moveDown_button.Click += new System.EventHandler(this.moveDown_button_Click);
            // 
            // moveLeft_button
            // 
            this.moveLeft_button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.moveLeft_button.Enabled = false;
            this.moveLeft_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.moveLeft_button.Location = new System.Drawing.Point(3, 58);
            this.moveLeft_button.Name = "moveLeft_button";
            this.moveLeft_button.Size = new System.Drawing.Size(58, 49);
            this.moveLeft_button.TabIndex = 0;
            this.moveLeft_button.Text = "←";
            this.moveLeft_button.UseVisualStyleBackColor = true;
            this.moveLeft_button.Click += new System.EventHandler(this.moveLeft_button_Click);
            // 
            // calibrate_button
            // 
            this.calibrate_button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calibrate_button.Enabled = false;
            this.calibrate_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calibrate_button.Location = new System.Drawing.Point(67, 58);
            this.calibrate_button.Name = "calibrate_button";
            this.calibrate_button.Size = new System.Drawing.Size(58, 49);
            this.calibrate_button.TabIndex = 1;
            this.calibrate_button.Text = "+";
            this.calibrate_button.UseVisualStyleBackColor = true;
            this.calibrate_button.Click += new System.EventHandler(this.calibrate_button_Click);
            // 
            // logo_button
            // 
            this.logo_button.AccessibleDescription = "about box";
            this.logo_button.Cursor = System.Windows.Forms.Cursors.Help;
            this.logo_button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.logo_button.FlatAppearance.BorderSize = 0;
            this.logo_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.logo_button.Image = global::Makroskop.Properties.Resources.ipe_logo;
            this.logo_button.Location = new System.Drawing.Point(655, 3);
            this.logo_button.Name = "logo_button";
            this.logo_button.Size = new System.Drawing.Size(194, 59);
            this.logo_button.TabIndex = 6;
            this.logo_button.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.logo_button.UseVisualStyleBackColor = true;
            this.logo_button.Click += new System.EventHandler(this.logo_button_Click);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel4.ColumnCount = 4;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11F));
            this.tableLayoutPanel4.Controls.Add(this.label6, 3, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(200, 100);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(178, 5);
            this.label6.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(22, 95);
            this.label6.TabIndex = 3;
            this.label6.Text = "mm";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBox3
            // 
            this.textBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox3.Location = new System.Drawing.Point(3, 3);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(59, 20);
            this.textBox3.TabIndex = 0;
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 1;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Controls.Add(this.tableLayoutPanel1, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.statusStrip1, 0, 1);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 2;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(858, 612);
            this.tableLayoutPanel9.TabIndex = 2;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripProgressBar1,
            this.toolStripStatusLabel1,
            this.serial_StatusLabel,
            this.errors_toolStripStatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 587);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(858, 25);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(100, 19);
            this.toolStripProgressBar1.Step = 1;
            this.toolStripProgressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.AutoSize = false;
            this.toolStripStatusLabel1.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.toolStripStatusLabel1.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(400, 20);
            this.toolStripStatusLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // serial_StatusLabel
            // 
            this.serial_StatusLabel.AutoSize = false;
            this.serial_StatusLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.serial_StatusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.serial_StatusLabel.Name = "serial_StatusLabel";
            this.serial_StatusLabel.Size = new System.Drawing.Size(200, 20);
            this.serial_StatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // errors_toolStripStatusLabel
            // 
            this.errors_toolStripStatusLabel.AutoSize = false;
            this.errors_toolStripStatusLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.errors_toolStripStatusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.errors_toolStripStatusLabel.Name = "errors_toolStripStatusLabel";
            this.errors_toolStripStatusLabel.Size = new System.Drawing.Size(200, 20);
            // 
            // Scanning_backgroundWorker
            // 
            this.Scanning_backgroundWorker.WorkerReportsProgress = true;
            this.Scanning_backgroundWorker.WorkerSupportsCancellation = true;
            this.Scanning_backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.Scanning_backgroundWorker_DoWork);
            this.Scanning_backgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.Scanning_backgroundWorker_RunWorkerCompleted);
            this.Scanning_backgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.Scanning_backgroundWorker_ProgressChanged);
            // 
            // redraw_timer
            // 
            this.redraw_timer.Interval = 300;
            this.redraw_timer.Tick += new System.EventHandler(this.redraw_timer_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(858, 612);
            this.Controls.Add(this.tableLayoutPanel9);
            this.Controls.Add(this.main_left_panel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(638, 474);
            this.Name = "Form1";
            this.Text = "Makroskop";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.SizeChanged += new System.EventHandler(this.Form1_SizeChanged);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainB_pictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.main_pictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scale_pictureBox)).EndInit();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel main_left_panel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TextBox xSize_textBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TextBox pocetPxX_textBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox velocity_textBox;
        private System.Windows.Forms.Button scan_button;
        private System.Windows.Forms.PictureBox main_pictureBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.PictureBox scale_pictureBox;
        private ZedGraph.ZedGraphControl zedGraphControl1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.RadioButton lineTool_radioButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel serial_StatusLabel;
        private System.ComponentModel.BackgroundWorker Scanning_backgroundWorker;
        private System.Windows.Forms.ToolStripStatusLabel errors_toolStripStatusLabel;
        private System.Windows.Forms.Timer redraw_timer;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.Button moveTop_button;
        private System.Windows.Forms.Button moveRight_button;
        private System.Windows.Forms.Button moveDown_button;
        private System.Windows.Forms.Button moveLeft_button;
        private System.Windows.Forms.Button calibrate_button;
        private System.Windows.Forms.PictureBox mainB_pictureBox;
        private System.Windows.Forms.Button logo_button;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.CheckBox opakovat_checkBox;
        private System.Windows.Forms.CheckBox return_checkBox;
        private System.Windows.Forms.Button button1;

    }
}

