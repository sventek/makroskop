﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Threading;

namespace Makroskop
{
    class Serial
    {

        #region systemove promenne
        /// <summary>
        /// Instance of serial port
        /// </summary>
        private SerialPort comport = new SerialPort();
        /// <summary>
        /// Instance of timer used to waiting during opening,writing,...
        /// </summary>
        private System.Timers.Timer waitTimer = new System.Timers.Timer(500);
        /// <summary>
        /// Wait flag - 0 wait, 1 OK, else error
        /// </summary>
        private Int16 waitFlag = 0;

        private volatile Int32 center_x = 0;
        private volatile Int32 center_y = 0;
        #endregion

        #region nastavení
        private string portName = "";
        public string PortName
        {
            get { return this.portName; }
        }

        public void setCenter(Int32 Xcenter, Int32 Ycenter)
        {
            center_x = Xcenter;
            center_y = Ycenter;
        }

        /// <summary>
        /// Setting communication speed
        /// </summary>
        public Int32 Baudrate
        {
            get { return comport.BaudRate; }
            set { comport.BaudRate = value; }
        }
        public string Status
        {
            get
            {
                if (portName == "")
                    return "Nepřipojeno";
                else
                    return "Připojeno";
            }
        }
        /// <summary>
        /// Received char from handshaking
        /// </summary>
        private const string StartHRxChar = "O";
        private const string StartHTxChar = "H";
        private const string StartRxChar = "P";
        private const string StartTxChar = "M";
        private const char StartManualModeChar = '1';
        private const char EndManualModeChar = '0';
        private const char PositionCoordsChar = '0';
        private const char VelocityCoordsChar = '1';
        #endregion

        #region Connection functions
        /// <summary>
        /// try to connect to all available ports
        /// </summary>
        public void Connect()
        {
            // try connect
            FindRightPort();
        }
        private Int16 FindRightPort()
        {
            // set up data receiver for handshake
            comport.DataReceived += new SerialDataReceivedEventHandler(handShakeRx);
            // set up timer overflow handler
            waitTimer.Elapsed += new System.Timers.ElapsedEventHandler(waitTimerElapsed);

            // get com port names
            string[] ports = SerialPort.GetPortNames();

            foreach (string s in ports)
            {
                // set a new portname
                comport.PortName = s;
                // try to open
                try
                {
                    comport.Open();
                }
                catch
                {
                    Console.WriteLine("Unable to open port " + s);
                    continue;
                }

                // try to write
                Thread send = new Thread(handShakeTx);
                send.Start();

                // start timer
                waitFlag = 0;
                waitTimer.Start();
                while (waitFlag == 0) ;

                // try to close
                if (comport.IsOpen)
                {
                    // erase buffer
                    comport.DiscardOutBuffer();

                    // stop timer
                    if (waitTimer.Enabled)
                        waitTimer.Stop();

                    // force stop sending (takes too long)
                    if (!send.IsAlive)
                        send.Abort();

                    // close
                    try
                    {
                        comport.Close();
                    }
                    catch { }
                }

                if (waitFlag == 1)
                {
                    portName = s;
                    break;
                }
            }

            // unset handshaker
            comport.DataReceived -= new SerialDataReceivedEventHandler(handShakeRx);

            // no openable port
            if (portName == "")
                return 0;
            else
            {
                // open right port
                comport.PortName = portName;
                comport.Open();
 
                // inform
                return 1;
            }
        }
        private void handShakeRx(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                while (comport.BytesToRead > 0)
                {
                    Char c = Convert.ToChar(comport.ReadChar());
                    Console.WriteLine(c);
                    if (c == StartHRxChar[0])
                        waitFlag = 1;
                    else
                        waitFlag = 3;
                }
            }
            catch
            {
                waitFlag = 3;
            }
        }
        private void handShakeTx()
        {
            try
            {
                comport.Write(StartHTxChar);
            }
            catch
            {
                Console.WriteLine("Cant send message on port " + comport.PortName);
            }
        }
        private void waitTimerElapsed(object sender, EventArgs e)
        {
            waitFlag = 2;
            waitTimer.Stop();
        }
        #endregion

        public ReceivedData getPossition()
        {
            comport.Write("S");
            return getValues();
        }

        public ReceivedData setPossition(Int32 X, Int32 Y, Int32 velocity)
        {
            string message = doubleToHex(X + 0x7FFFF + center_x, 5) + doubleToHex(Y + 0x7FFFF + center_y, 5) + doubleToHex(velocity, 2);

            // spocteme kontrolni soucet
            int xor = 'x';
            //for (int i = 0; i < message.Length; i++)
            //{
            //    xor ^= message[i];
            //}

            // doplnime o start bit a kontrolni soucet
            message = StartTxChar + message + (char)xor;
            try
            {
                comport.Write(message);
                return getValues();
            }
            catch
            {
                return new ReceivedData(false);
            }
        }

        private ReceivedData getValues()
        {
            List<char> received = new List<char> { };
            
            while (received.Count < 19)
            {
                if (comport.BytesToRead > 0)
                {
                    Char c = Convert.ToChar(comport.ReadChar());
                    if (c == StartRxChar[0])
                        received.Clear();
                    else
                        received.Add(c);
                }
            }
            return processMessage(received);
        }

        private ReceivedData processMessage(List<char> received)
        {
            // make a string from list of chars
            string s_pos_y = "", s_pos_x = "", s_hal = "", s_iA = "", s_iB = "";
            for (int i = 0; i < received.Count; i++)
            {
                if (i < 5)
                    s_pos_x += received[i];
                else if (i < 10)
                    s_pos_y += received[i];
                else if (i < 13)
                    s_hal += received[i];
                else if (i < 16)
                    s_iA += received[i];
                else if (i < 19)
                    s_iB += received[i];
            }
            Int32 pos_x, pos_y, hal, iA, iB;
            pos_x = hex2dec(s_pos_x) - 0x7FFFF - center_x;
            pos_y = hex2dec(s_pos_y) - 0x7FFFF - center_y;
            hal = hex2dec(s_hal);
            iA = hex2dec(s_iA);
            iB = hex2dec(s_iB);

            return new ReceivedData(pos_x, pos_y, hal, iA, iB, true);
        }

        #region hex funkce
        private Int32 hex2dec(string hex)
        {
            Int32 value = 0;
            for (int i = 0; i < hex.Length; i++)
            {
                if (hex[i] <= '9')
                    value += (hex[i] - '0') << (4 * ((hex.Length - 1) - i));
                else
                    value += ((hex[i] - 'a') + 10) << (4 * ((hex.Length - 1) - i));
            }
            return value;
        }
        private string doubleToHex(double value, int num)
        {
            string hex = "";
            Int32 val_int = Convert.ToInt32(value);
            for (int i = (num - 1); i >= 0; i--)
            {
                Int32 val_i = (val_int >> (i * 4)) & 0x0F;
                char add;
                if (val_i < 10)
                    add = Convert.ToChar('0' + val_i);
                else
                    add = Convert.ToChar('a' + (val_i - 10));
                hex += add;
            }
            return hex;
        }
        private bool isHex(char value)
        {
            if (('0' <= value && value <= '9') || ('A' <= value && value <= 'F'))
                return true;
            else
                return false;
        }
        #endregion
    }
    public struct ReceivedData
    {
        public Int32 X, Y, hal, iA, iB;
        public Boolean valid;

        public ReceivedData(Int32 nX, Int32 nY, Int32 nhal, Int32 niA, Int32 niB, Boolean nvalid)
        {
            X = nX;
            Y = nY;
            hal = nhal;
            iA = niA;
            iB = niB;
            valid = nvalid;
        }
        public ReceivedData(Boolean nvalid)
        {
            X = 0;
            Y = 0;
            hal = 0;
            iA = 0;
            iB = 0;
            valid = nvalid;

        }
    }
}
