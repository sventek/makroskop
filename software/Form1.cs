﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZedGraph;

namespace Makroskop
{
    public partial class Form1 : Form
    {
        // global variables
        GraphPane graph = new GraphPane();
        PointPairList graphData = new PointPairList();
        LineItem graphLine;
        Image img_backup; // backup for displaying line tool
        DataSet scaned_imageA;
        DataSet scaned_imageB;
        Serial serial;
        private int tolerance = 80; // tolerance regulacni odchylky
        private const Int32 encPerMm = 240;
        // global flags
        int lineTool = 0;
        Point lineTool_A;
        Point lineTool_B;
        private bool isScanning = false;
        private bool canceled = false;

        System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();

        Int32 manual_step = 1000;

        #region parametry skenu
        Int32 size_PX;
        double size_mm;
        Int32 stepEnc;
        #endregion

        public Form1()
        {
            InitializeComponent();

            // temp> display img after start
            //int size_X_px = main_pictureBox.Size.Width;
            //int size_Y_px = main_pictureBox.Size.Height;
            //scaned_image = new DataSet(400, 400);
            //scaned_image.AtomGenerator();
            //scaned_image.getMinMax();


            //main_pictureBox.Image = scaned_image.getImage(size_X_px, size_Y_px);
            //main_pictureBox.Invalidate();
            //scale_pictureBox.Image = scaned_image.createScale(scale_pictureBox.Size);
            //scale_pictureBox.Invalidate(); 

            // line graph settings
            graph = zedGraphControl1.GraphPane;
            graph.Title.IsVisible = false;
            graph.XAxis.Title.IsVisible = false;
            graph.XAxis.Scale.FontSpec.Size = 25;
            graph.YAxis.Title.IsVisible = false;
            graph.YAxis.Scale.FontSpec.Size = 25;
            graph.AxisChange();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            #region připojení sériáku
            // obsluha událostí serial portu
            serial = new Serial();
            serial.Baudrate = 76800;
            serial.Connect();
            if (serial.PortName.Length > 0)
            {
                serial_StatusLabel.Text = "Připojeno";
                greyAllManual(true);
            }
            else
                serial_StatusLabel.Text = "Nepodařilo se najít port";
            #endregion

            Int32 pocPX_x = Convert.ToInt32(pocetPxX_textBox.Text);

            scaned_imageA = new DataSet(pocPX_x, pocPX_x);
            scaned_imageA.EmptyDataset();

            scaned_imageB = new DataSet(pocPX_x, pocPX_x);
            scaned_imageB.EmptyDataset();

        }

        private void scan_button_Click(object sender, EventArgs e)
        {
            canceled = false;
            startMeasure();
        }

        private void startMeasure()
        {
            if (!isScanning)
            {
                size_PX = getSizeX();
                size_mm = getSizeMM();
                stepEnc = Convert.ToInt32((double)encPerMm * size_mm / (double)size_PX);
                errors_toolStripStatusLabel.Text = "";
                try
                {
                    scaned_imageA = new DataSet(size_PX, size_PX);
                    scaned_imageA.dataset_size_mm = getSizeMM();
                    scaned_imageA.EmptyDataset();

                    scaned_imageB = new DataSet(size_PX, size_PX);
                    scaned_imageB.dataset_size_mm = getSizeMM();
                    scaned_imageB.EmptyDataset();
                }
                catch
                {
                    errors_toolStripStatusLabel.Text = "Zadaný počet není číslo!";
                }

                redraw_timer.Enabled = true;
                //SerialComm.GoTo(Convert.ToInt32(xSize_textBox.Text), Convert.ToInt32(ySize_textBox.Text), Convert.ToInt32(velocity_textBox.Text));
                Scanning_backgroundWorker.RunWorkerAsync();

                isScanning = true;

                scan_button.Text = "Zastavit!";
                scan_button.BackColor = Color.FromArgb(255, 192, 192);

                greyAllManual(false);
            }
            else
            {
                canceled = true;
                try
                {
                    Scanning_backgroundWorker.CancelAsync();
                }
                catch
                { }
                isScanning = false;

                scan_button.Text = "Skenovat!";
                scan_button.BackColor = Color.FromArgb(192, 255, 192);
                greyAllManual(true);
            }
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            try
            {
                int size_X_px_A = main_pictureBox.Size.Width;
                int size_Y_px_A = main_pictureBox.Size.Height;

                main_pictureBox.Image = scaned_imageA.getImage(size_X_px_A, size_Y_px_A);
                main_pictureBox.Invalidate();

                int size_X_px_B = mainB_pictureBox.Size.Width;
                int size_Y_px_B = mainB_pictureBox.Size.Height;

                mainB_pictureBox.Image = scaned_imageB.getImage(size_X_px_B, size_Y_px_B);
                mainB_pictureBox.Invalidate();

                scale_pictureBox.Image = scaned_imageA.createScale(scale_pictureBox.Size);
                scale_pictureBox.Invalidate();
            }
            catch
            { }
        }

        private void lineTool_radioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (lineTool_radioButton.Checked == true)
            {
                img_backup = (Image)main_pictureBox.Image.Clone();
                lineTool = 1;
            }
            else // unpressed 
            {
                lineTool = 0;
            }
        }

        #region main_pictureBox Mouse events
        private void main_pictureBox_mouseMove(object sender, MouseEventArgs e)
        {
            dimm d = scaned_imageA.getImgCoords(main_pictureBox.Width, main_pictureBox.Height);
            int eX = e.X;
            int eY = e.Y;

            if (d.x_min > eX)
                eX = d.x_min;
            if (d.y_min > eY)
                eY = d.y_min;
            if (d.x_max < eX)
                eX = d.x_max;
            if (d.y_max < eY)
                eY = d.y_max;

            if (lineTool == 1)
            {
                Image img_temp = (Image)img_backup.Clone();
                Graphics g = Graphics.FromImage(img_temp);
                Pen red = new Pen(Color.IndianRed, 2);
                g.DrawEllipse(red, eX - 5, eY - 5, 10, 10);
                main_pictureBox.Image = img_temp;
                main_pictureBox.Invalidate();

            }

            if (lineTool == 2) // point A is placed
            {
                Image img_temp = (Image)img_backup.Clone();
                Graphics g = Graphics.FromImage(img_temp);
                Pen red = new Pen(Color.IndianRed, 2);
                g.DrawLine(red, lineTool_A, new Point(eX, eY));
                g.DrawEllipse(red, lineTool_A.X - 5, lineTool_A.Y - 5, 10, 10);
                g.DrawEllipse(red, eX - 5, eY - 5, 10, 10);
                main_pictureBox.Image = img_temp;
                main_pictureBox.Invalidate();
            }
        }

        private void main_pictureBox_MouseClick(object sender, MouseEventArgs e)
        {
            dimm d = scaned_imageA.getImgCoords(main_pictureBox.Width, main_pictureBox.Height);
            int eX = e.X;
            int eY = e.Y;

            if (d.x_min > eX)
                eX = d.x_min;
            if (d.y_min > eY)
                eY = d.y_min;
            if (d.x_max < eX)
                eX = d.x_max;
            if (d.y_max < eY)
                eY = d.y_max;

            if (lineTool == 1)
            {
                lineTool_A = new Point(eX, eY);
                lineTool = 2;
            }
            else if (lineTool == 2)
            {
                lineTool_B = new Point(eX, eY);

                // display profile
                int xA, xB, yA, yB;
                double x_scale = ((double)d.x_max - (double)d.x_min) / (double)scaned_imageA.dataset_size_x;
                double y_scale = ((double)d.y_max - (double)d.y_min) / (double)scaned_imageA.dataset_size_y;

                xA = Convert.ToInt32((lineTool_A.X - d.x_min) / x_scale);
                xB = Convert.ToInt32((lineTool_B.X - d.x_min) / x_scale);

                yA = Convert.ToInt32((lineTool_A.Y - d.y_min) / y_scale);
                yB = Convert.ToInt32((lineTool_B.Y - d.y_min) / y_scale);

                int length = Convert.ToInt32(Math.Sqrt(Math.Pow((double)(xB - xA), 2) + Math.Pow(yB - yA, 2)));
                graph.XAxis.Scale.Min = 0;
                graph.XAxis.Scale.Max = length;

                //graphLine.Clear();
                x_scale = (double)((xB - xA)) / (double)length;
                y_scale = (double)((yB - yA)) / (double)length;

                try
                {
                    graphData.Clear();
                }
                catch { }

                double min = 0;
                double max = 0;

                for (int i = 0; i < length; i++)
                {
                    int x = xA + Convert.ToInt32(Math.Round(x_scale * (double)i));
                    int y = yA + Convert.ToInt32(Math.Round(y_scale * (double)i));

                    double val = scaned_imageA.ConvertData2mm(x, y);
                    if (min > val)
                        min = val;
                    if (max < val)
                        max = val;

                    graphData.Add(i, val);
                }
                graph.CurveList.Clear();
                graphLine = graph.AddCurve(null, graphData, Color.IndianRed, SymbolType.None);

                graph.YAxis.Scale.MinAuto = true;
                graph.YAxis.Scale.MaxAuto = true;

                zedGraphControl1.Invalidate();
                zedGraphControl1.AxisChange();
                lineTool = 3;
            }
            else if (lineTool == 3)
            {
                lineTool_A = new Point(eX, eY);
                lineTool = 2;
            }
        }
        #endregion

        #region skenovani
        private void Scanning_backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            // zapneme stopky
            stopwatch.Reset();
            stopwatch.Start();
            
            int num_points = 2 * size_PX * size_PX;
            int i = 0;

            graph.CurveList.Clear();
            graph.YAxis.Scale.MinAuto = true;
            graph.YAxis.Scale.MaxAuto = true;

            for (int y = 0; y < size_PX; y++)
            {
                PointPairList graphDataA = new PointPairList();
                PointPairList graphDataB = new PointPairList();
                for (int x = 0; x < size_PX; x++)
                {
                    int x_pos = x * stepEnc;
                    int y_pos = y * stepEnc;

                    if (!Scanning_backgroundWorker.CancellationPending)
                    {
                        //Console.WriteLine("Požadujeme [" + (x_pos).ToString() + ";" + (y_pos).ToString() + "]");
                        Int32 error = tolerance;

                        ReceivedData data = new ReceivedData(false);
                        while (error >= tolerance)
                        {
                            data = serial.setPossition(x_pos, y_pos, getVelocity());
                            error = Convert.ToInt32(Math.Sqrt(
                                        Math.Pow(x_pos - data.X, 2) +
                                        Math.Pow(y_pos - data.Y, 2)
                                ));
                        }

                        scaned_imageA.setDataPoint(x, y, data.hal);
                        graphDataA.Add((double)x_pos / (double)encPerMm, DataSet.ConvertData2mm(data.hal));
                        //errors_toolStripStatusLabel.Text = "[" + x_pos.ToString() + "; " + y_pos.ToString() + "] > " + data.hal.ToString();

                        i++;
                        int progress = Convert.ToInt32(Math.Round(Convert.ToDouble(100 * i / num_points)));
                        Scanning_backgroundWorker.ReportProgress(progress);
                    }
                }
                for (int x = (size_PX - 1); x >= 0; x--)
                {
                    int x_pos = x * stepEnc;
                    int y_pos = y * stepEnc;

                    if (!Scanning_backgroundWorker.CancellationPending)
                    {
                        //Console.WriteLine("Požadujeme [" + (x_pos).ToString() + ";" + (y_pos).ToString() + "]");
                        Int32 error = tolerance;

                        ReceivedData data = new ReceivedData(false);
                        while (error >= tolerance)
                        {
                            data = serial.setPossition(x_pos, y_pos, getVelocity());
                            error = Convert.ToInt32(Math.Sqrt(
                                        Math.Pow(x_pos - data.X, 2) +
                                        Math.Pow(y_pos - data.Y, 2)
                                ));
                        }

                        scaned_imageB.setDataPoint(x, y, data.hal);
                        graphDataB.Add((double)x_pos / (double)encPerMm, DataSet.ConvertData2mm(data.hal));
                        //errors_toolStripStatusLabel.Text = "[" + x_pos.ToString() + "; " + y_pos.ToString() + "] > " + data.hal.ToString();


                        i++;
                        int progress = Convert.ToInt32(Math.Round(Convert.ToDouble(100 * i / num_points)));
                        Scanning_backgroundWorker.ReportProgress(progress);
                    }
                }

                // vykreslime graf
                graph.CurveList.Clear();

                LineItem lineA = graph.AddCurve(null, graphDataA, Color.IndianRed, SymbolType.None);
                LineItem lineB = graph.AddCurve(null, graphDataB, Color.CadetBlue, SymbolType.None);

                graph.AxisChange();
                zedGraphControl1.Invalidate();
                zedGraphControl1.AxisChange();
            }

            if (return_checkBox.Checked)
                serial.setPossition(0, 0, getVelocity());
        }

        private void Scanning_backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            toolStripProgressBar1.Value = e.ProgressPercentage;

            double milisec = Convert.ToDouble(stopwatch.ElapsedMilliseconds);
            double percent = Convert.ToDouble(e.ProgressPercentage)/100;
            string elapsed = timeToStr(milisec);
            string remain = timeToStr((1 - percent) * stopwatch.ElapsedMilliseconds / percent);

            toolStripStatusLabel1.Text = e.ProgressPercentage.ToString() + " % | Uběhlo: " + elapsed + " | Zbývá: " + remain;
            // doplnit casovy odhad

        }

        private void Scanning_backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            toolStripStatusLabel1.Text = "Dokončeno";
            redraw_timer.Enabled = false;

            scan_button.Text = "Skenovat!";
            scan_button.BackColor = Color.FromArgb(192, 255, 192);
            greyAllManual(true);
            reDraw();
            isScanning = false;

            if (opakovat_checkBox.Checked && !canceled)
                startMeasure();
        }

        private void redraw_timer_Tick(object sender, EventArgs e)
        {
            reDraw();
        }

        private void reDraw()
        {
            int size_X_px = main_pictureBox.Size.Width;
            int size_Y_px = main_pictureBox.Size.Height;

            main_pictureBox.Image = scaned_imageA.getImage(size_X_px, size_Y_px);
            main_pictureBox.Invalidate();

            mainB_pictureBox.Image = scaned_imageB.getImage(size_X_px, size_Y_px);
            mainB_pictureBox.Invalidate();

            scale_pictureBox.Image = scaned_imageA.createScale(scale_pictureBox.Size);
            scale_pictureBox.Invalidate();
        }
        #endregion

        #region manualni ovladani
        private void moveTop_button_Click(object sender, EventArgs e)
        {
            ReceivedData data = serial.getPossition();
            serial.setPossition(data.X + manual_step, data.Y, getVelocity());

            //SerialComm.GoTo(current_x_enc + manual_step, current_y_enc, 10);
        }
        private void moveRight_button_Click(object sender, EventArgs e)
        {
            ReceivedData data = serial.getPossition();
            serial.setPossition(data.X, data.Y + manual_step, getVelocity());
            //SerialComm.GoTo(current_x_enc, current_y_enc + manual_step, 10);
        }
        private void moveDown_button_Click(object sender, EventArgs e)
        {
            ReceivedData data = serial.getPossition();
            serial.setPossition(data.X - manual_step, data.Y, getVelocity());
            //SerialComm.GoTo(current_x_enc - manual_step, current_y_enc, 10);
        }
        private void moveLeft_button_Click(object sender, EventArgs e)
        {
            ReceivedData data = serial.getPossition();
            serial.setPossition(data.X, data.Y - manual_step, getVelocity());
        }
        private void calibrate_button_Click(object sender, EventArgs e)
        {
            ReceivedData data = serial.getPossition();
            errors_toolStripStatusLabel.Text = "Střed nastaven na [" + data.X.ToString() + "; " + data.Y.ToString() + "]";
            serial.setCenter(data.X, data.Y);
            if (!scan_button.Enabled)
                scan_button.Enabled = true;
        }
        #endregion

        #region nacitani hodnot z formularu
        private Int32 getVelocity()
        {
            try
            {
                return Convert.ToInt32(velocity_textBox.Text);
            }
            catch
            {
                errors_toolStripStatusLabel.Text = "Zadejte rychlost jako číslo!";
                return 0;
            }
        }
        private double getSizeMM()
        {
            try
            {
                return Convert.ToInt32(xSize_textBox.Text);
            }
            catch
            {
                errors_toolStripStatusLabel.Text = "Zadejte velikost jako číslo!";
                return 0;
            }
        }
        private Int32 getSizeX()
        {
            try
            {
                return Convert.ToInt32(pocetPxX_textBox.Text);
            }
            catch
            {
                errors_toolStripStatusLabel.Text = "Zadejte velikost jako číslo!";
                return 0;
            }
        }
        #endregion

        #region pomcne funkce
        private void greyAllManual(bool enabled)
        {
            moveLeft_button.Enabled = enabled;
            moveRight_button.Enabled = enabled;
            moveTop_button.Enabled = enabled;
            moveDown_button.Enabled = enabled;
            calibrate_button.Enabled = enabled;
        }
        private string timeToStr(double time)
        {
            time = time / Convert.ToDouble(1000);
            string str = "";

            if (double.IsInfinity(time))
                return "∞";

            double hodin = Math.Floor(time / 3600);
            if (hodin >= 1)
            {
                str += hodin.ToString() + " " + sklonuj(hodin, "hodin", "hodina", "hodiny")+" ";
                time -= hodin * 3600;
            }

            double minut = Math.Floor(time / 60);
            if (minut >= 1)
            {
                str += minut.ToString() + " " + sklonuj(minut, "minut", "minuta", "minuty")+" ";
                time -= minut * 60;
            }

            time = Math.Floor(time);
            if (time > 0)
                str += time.ToString() + " " + sklonuj(time, "sekund", "sekunda", "sekundy");

            return str;
        }
        private string sklonuj(double hodnota, string sekund, string sekunda, string sekundy)
        {
            if (hodnota == 0 || hodnota > 4)
                return sekund;
            else if (hodnota == 1)
                return sekunda;
            else
                return sekundy;
        }
        #endregion

        // vykresli domecek jednim tahem
        private void easterEggA()
        {
            ReceivedData data = serial.getPossition();
            serial.setPossition(data.X, data.Y, getVelocity());
            System.Threading.Thread.Sleep(1000);
            serial.setPossition(data.X, data.Y + 64800, getVelocity());
            System.Threading.Thread.Sleep(1000);
            serial.setPossition(data.X + 4800, data.Y + 4800, getVelocity());
            System.Threading.Thread.Sleep(1000);
            serial.setPossition(data.X + 4800, 0, getVelocity());
            System.Threading.Thread.Sleep(1000);
            serial.setPossition(data.X, data.Y, getVelocity());
            System.Threading.Thread.Sleep(1000);
            serial.setPossition(data.X + 4800, data.Y + 4800, getVelocity());
            System.Threading.Thread.Sleep(1000);
            serial.setPossition(data.X + 2400, data.Y + 7200, getVelocity());
            System.Threading.Thread.Sleep(1000);
            serial.setPossition(data.X, data.Y + 4800, getVelocity());
            System.Threading.Thread.Sleep(1000);
            serial.setPossition(data.X + 4800, data.Y, getVelocity());
        }

        private void logo_button_Click(object sender, EventArgs e)
        {
            AboutBox1 about = new AboutBox1();
            about.ShowDialog();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            Console.WriteLine(e.KeyCode);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            scaned_imageA.PrintDataset();
            scaned_imageB.PrintDataset();
        }
    }
}
