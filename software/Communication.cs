﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Threading;

namespace Makroskop
{
    static class SerialComm
    {
        #region systémové proměnné
        /// <summary>
        /// Instance of serial port
        /// </summary>
        private static SerialPort comport = new SerialPort();
        /// <summary>
        /// Instance of timer used to waiting during opening,writing,...
        /// </summary>
        private static System.Timers.Timer waitTimer = new System.Timers.Timer(200);
        private static System.Timers.Timer tryConnectTimer = new System.Timers.Timer(10000);
        /// <summary>
        /// Wait flag - 0 wait, 1 OK, else error
        /// </summary>
        private static Int16 waitFlag = 0;
        private static List<char> received = new List<char> { };
        public static Form1 form;
        private static int messageCounter = 0;

        private static volatile Int32 center_x = 0; 
        private static volatile Int32 center_y = 0;

        #endregion

        #region nastavení
        private static string portName = "";
        public static string PortName
        {
            get { return SerialComm.portName; }
        }

        public static void setCenter(Int32 Xcenter, Int32 Ycenter)
        {
            center_x = Xcenter;
            center_y = Ycenter;
        }

        /// <summary>
        /// Setting communication speed
        /// </summary>
        public static Int32 Baudrate
        {
            get { return comport.BaudRate; }
            set { comport.BaudRate = value; }
        }
        public static string Status
        {
            get
            {
                if (portName == "")
                    return "Nepřipojeno";
                else
                    return "Připojeno";
            }
        }
        /// <summary>
        /// Received char from handshaking
        /// </summary>
        private const string StartHRxChar = "O";
        private const string StartHTxChar = "H";
        private const string StartRxChar = "P";
        private const string StartTxChar = "M";
        private const char StartManualModeChar = '1';
        private const char EndManualModeChar = '0';
        private const char PositionCoordsChar = '0';
        private const char VelocityCoordsChar = '1';

        private static int tolerance = 100; // tolerance regulační odchylky        
        #endregion

        #region aktuální posun
        private static Int32 X;
        private static Int32 Y;
        private static Int32 velocity;
        private static string controllType;
        #endregion


        #region akce a delegáty
        // událost po navázání připojení
        public delegate void ConnectionEstablishedDelegate(object sender, ActionSerialComm ev);
        public static event ConnectionEstablishedDelegate Connected;
        public class ActionSerialComm : System.ComponentModel.CancelEventArgs
        {
            public ActionSerialComm() : this(false) { }

            public ActionSerialComm(bool cancel) : this(false, string.Empty) { }

            public ActionSerialComm(bool cancel, string message)
                : base(cancel)
            {
                this.Message = message;
            }
            public string Message { get; set; }

        }
        // událost po ztrátě připojení
        public delegate void ConnectionLostDelegate(object sender, ActionSerialComm ev);
        public static event ConnectionLostDelegate ConnectionLost;
        // událost pro dojeti na polohu
        public delegate void InPlaceDelegate(object sender, ActionInPlace ev);
        public static event InPlaceDelegate InPlace;
        public class ActionInPlace : System.ComponentModel.CancelEventArgs
        {
            public Int32 Xpos { get; set; }
            public Int32 Ypos { get; set; }
            public Int32 hal_adc { get; set; }
            public Int32 iA_adc { get; set; }
            public Int32 iB_adc { get; set; }
            public ActionInPlace(Int32 pos_X, Int32 pos_Y, Int32 hal, Int32 iA, Int32 iB)
            {
                this.Xpos = pos_X;
                this.Ypos = pos_Y;

                this.hal_adc = hal;

                this.iA_adc = iA;
                this.iB_adc = iB;
            }
        }
        // událost pro změnu manual režimu
        public delegate void ManualModeChangedDelegate(object sender, ManualModeAction ev);
        public static event ManualModeChangedDelegate ManualModeChanged;
        public class ManualModeAction : System.ComponentModel.CancelEventArgs
        {
            public bool On { get; set; }
            public ManualModeAction(bool isOn)
            {
                this.On = isOn;
            }
        }
        private static bool ManualMode = false;
        // událost pro předání aktuální pozice
        public delegate void AtPositionDelegate(object sender, ActionInPlace ev);
        public static event AtPositionDelegate AtPosition;
        #endregion

        #region Connection functions
        /// <summary>
        /// try to connect to all available ports
        /// </summary>
        public static void Connect()
        {
            // init stuff
            tryConnectTimer.Elapsed += new System.Timers.ElapsedEventHandler(tryConnectTimer_Elapsed);

            // try connect
            if (FindRightPort() == 1)
            {
                // vytvoříme událost "připojeno"
                Connected(SerialComm.form, new ActionSerialComm());
            }
            else
            {
                // zapneme časovač, který bude zkoušet připojení dál
                tryConnectTimer.Start();
            }

        }

        private static void tryConnectTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            // zkusíme se připojit
            if (FindRightPort() == 1)
            {
                // vytvoříme událost a vypneme časovač
                Connected(SerialComm.form, new ActionSerialComm());
                tryConnectTimer.Stop();
            }
            else
            {
                Console.WriteLine("Vhodný port nenalezen!");
            }
        }
        private static Int16 FindRightPort()
        {
            // set up data receiver for handshake
            comport.DataReceived += new SerialDataReceivedEventHandler(handShakeRx);
            // set up timer overflow handler
            waitTimer.Elapsed += new System.Timers.ElapsedEventHandler(waitTimerElapsed);

            // get com port names
            string[] ports = SerialPort.GetPortNames();
            Console.WriteLine(ports);
            foreach (string s in ports)
            {
                // set a new portname
                comport.PortName = s;
                // try to open
                try
                {
                    comport.Open();
                }
                catch
                {
                    Console.WriteLine("Unable to open port " + s);
                    continue;
                }

                // try to write
                Thread send = new Thread(handShakeTx);
                send.Start();

                // start timer
                waitFlag = 0;
                waitTimer.Start();
                while (waitFlag == 0) ;

                // try to close
                if (comport.IsOpen)
                {
                    // erase buffer
                    comport.DiscardOutBuffer();

                    // stop timer
                    if (waitTimer.Enabled)
                        waitTimer.Stop();

                    // force stop sending (takes too long)
                    if (!send.IsAlive)
                        send.Abort();

                    // close
                    try
                    {
                        comport.Close();
                    }
                    catch { }
                }

                if (waitFlag == 1)
                {
                    portName = s;
                    break;
                }
            }

            // unset handshaker
            comport.DataReceived -= new SerialDataReceivedEventHandler(handShakeRx);

            // no openable port
            if (portName == "")
                return 0;
            else
            {
                // open right port
                comport.PortName = portName;
                comport.Open();
                // set up new receiver
                comport.DataReceived += new SerialDataReceivedEventHandler(receiver);
                // inform
                return 1;
            }
        }
        private static void handShakeRx(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                while (comport.BytesToRead > 0)
                {
                    Char c = Convert.ToChar(comport.ReadChar());
                    if (c == StartHRxChar[0])
                        waitFlag = 1;
                    else
                        waitFlag = 3;
                }
            }
            catch
            {
                waitFlag = 3;
            }
        }
        private static void handShakeTx()
        {
            try
            {
                comport.Write(StartHTxChar);
            }
            catch
            {
                Console.WriteLine("Cant send message on port " + comport.PortName);
            }
        }
        private static void waitTimerElapsed(object sender, EventArgs e)
        {
            waitFlag = 2;
            waitTimer.Stop();
        }
        #endregion

        #region Globální odesílač a příjimač
        /// <summary>
        /// Odešle požadavek na přesun na zadanou polohu
        /// </summary>
        /// <param name="pos_x">Poloha v X ose</param>
        /// <param name="pos_y">Poloha v Y ose</param>
        /// <returns>Vrátí úspěšnost pokusu o odeslání</returns>
        public static bool GoTo(Int32 pos_x, Int32 pos_y, Int32 vel)
        {
            // uložíme si kam chceme jet
            X = pos_x;
            Y = pos_y;
            velocity = vel;
            controllType = PositionCoordsChar.ToString();

            // vytvorime zpravu
            string message = doubleToHex(pos_x + 0x7FFFF + center_x, 5) + doubleToHex(pos_y + 0x7FFFF + center_y, 5) + doubleToHex(velocity, 2);

            // spocteme kontrolni soucet
            int xor = 'x';
            //for (int i = 0; i < message.Length; i++)
            //{
            //    xor ^= message[i];
            //}

            // doplnime o start bit a kontrolni soucet
            message = StartTxChar + message + (char)xor;
            try
            {
                comport.Write(message);
                Console.WriteLine(message);
            }
            catch
            {
                return false;
            }
            return true;
        }
        /// <summary> 
        /// Odešle požadavek na posun uvedenou rychlostí
        /// </summary>
        /// <param name="vel_x">Rych</param>
        /// <param name="vel_y"></param>
        /// <returns>Vrací úspěšnost akce</returns>
        public static bool GoOn(Int32 vel_x, Int32 vel_y)
        {
            // uložíme si jak rychle chceme jet
            X = vel_x;
            Y = vel_y;
            controllType = VelocityCoordsChar.ToString();

            string message = StartTxChar + doubleToHex(vel_x,5) + doubleToHex(vel_y,5) + VelocityCoordsChar;
            try
            {
                comport.Write(message);
            }
            catch
            {
                ConnectionLost(SerialComm.form, new ActionSerialComm());
                tryConnectTimer.Start();
                return false;
            }
            return true;
        }

        private static void receiver(object sender, SerialDataReceivedEventArgs e)
        {
            while (comport.BytesToRead > 0)
            {
                Char c = Convert.ToChar(comport.ReadChar());

                //Console.Write(c);
                if (c == StartRxChar[0]) // start znak
                {
                    received.Clear();
                }
                else
                    received.Add(c);

                // complete message then process
                if (received.Count == 19)
                {
                    processMessage();
                    received.Clear();
                }
            }
        }
        #endregion

        #region pomocné funkce
        private static void processMessage()
        {

            // make a string from list of chars
            string s_pos_y = "", s_pos_x = "", s_hal = "", s_iA = "", s_iB = "";
            for (int i = 0; i < received.Count; i++)
            {
                Console.Write(received[i]);

                if (i < 5)
                    s_pos_x += received[i];
                else if (i < 10)
                    s_pos_y += received[i];
                else if (i < 13)
                    s_hal += received[i];
                else if (i < 16)
                    s_iA += received[i];
                else if (i < 19)
                    s_iB += received[i];
            }
            Console.Write('\n');
            Int32 pos_x, pos_y, hal, iA, iB;
            double hal2;
            pos_x = hex2dec(s_pos_x) - 0x7FFFF - center_x;
            pos_y = hex2dec(s_pos_y) - 0x7FFFF - center_y;
            hal2 = hal2mm(hex2dec(s_hal));
            iA = hex2dec(s_iA);
            iB = hex2dec(s_iB);
            Console.WriteLine(hal2);
            hal = Convert.ToInt32(hal2);
            //Console.WriteLine();
            //Console.WriteLine("aktuální poloha [" + pos_x.ToString() + "; " + pos_y.ToString() + "] = " + hal + "(A: " + iA.ToString() + " ; B: " + iB.ToString() + ")");
            // pokud došlo ke změně u zapnutí externího ovladače
           /* if (s_man == StartManualModeChar.ToString() && ManualMode != true)
            {   // pak předáme do hlavního formu
                ManualMode = true;
                ManualModeChanged(null, new ManualModeAction(ManualMode));
            }
            else if (s_man == EndManualModeChar.ToString() && ManualMode == true)
            {
                ManualMode = false;
                ManualModeChanged(null, new ManualModeAction(ManualMode));
            }*/

            // jestli jde o polohování
            if (controllType == PositionCoordsChar.ToString())
            {
                // pokud jsme v toleranci
                if (Math.Abs(X - pos_x) < tolerance) //&& Math.Abs(Y - pos_y) < tolerance)
                    InPlace(null, new ActionInPlace(X, Y, hal, iA, iB)); // vytvoříme událost
                GoTo(X, Y,velocity);
            }
            else if (controllType == VelocityCoordsChar.ToString())
                GoOn(X, Y);

            AtPosition(null, new ActionInPlace(pos_x, pos_y, hal, iA, iB));

            // brzda
            //Thread.Sleep(100);

            //Makroskop.mainForm.dr
            //Console.WriteLine("["+pos_x.ToString()+";"+pos_y.ToString()+"] -> "+adc.ToString());
        }

        private static double hal2mm(Int32 x)
        {
            
            double a0 = -6.748235905963436e-01;
            double a1 = 5.377627447295921e-02;
            double a2 = -1.402888941198118e-04;
            double a3 = 3.885917632292043e-07;

            return a0 + a1 * Convert.ToDouble(x) + a2 * Math.Pow(Convert.ToDouble(x), 2) + a3 * Math.Pow(Convert.ToDouble(x), 3);
        }

        private static Int32 hex2dec(string hex)
        {
            Int32 value = 0;
            for (int i = 0; i < hex.Length; i++)
            {
                if (hex[i] <= '9')
                    value += (hex[i] - '0') << (4 * ((hex.Length - 1) - i));
                else
                    value += ((hex[i] - 'a') + 10) << (4 * ((hex.Length - 1) - i));
            }
            return value;
        }

        private static string doubleToHex(double value,int num)
        {
            string hex = "";
            Int32 val_int = Convert.ToInt32(value);
            for (int i = (num-1); i >= 0; i--)
            {
                Int32 val_i = (val_int >> (i * 4)) & 0x0F;
                char add;
                if (val_i < 10)
                    add = Convert.ToChar('0' + val_i);
                else
                    add = Convert.ToChar('a' + (val_i - 10));
                hex += add;
            }
            return hex;
        }

        private static bool isHex(char value)
        {
            if (('0' <= value && value <= '9') || ('A' <= value && value <= 'F'))
                return true;
            else
                return false;
        }
        #endregion
    }
}
