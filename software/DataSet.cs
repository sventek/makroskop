﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

namespace Makroskop
{
    class DataSet
    {
        private int[,] dataset;
        public int dataset_size_x, dataset_size_y;
        public double dataset_size_mm;

        public DataSet(int size_x, int size_y)
        {
            dataset_size_x = size_x;
            dataset_size_y = size_y;
            dataset = new int[size_x, size_y];
        }

        private int min_value = 1023;
        private int max_value = 0;



        /// <summary>
        /// Generates fake measured data
        /// </summary>
        /// <param name="size_x">X size in px</param>
        /// <param name="size_y">Y size</param>
        /// <returns>fake measured dataset</returns>
        public void AtomGenerator()
        {
            int[,] fake_dataset = new int[dataset_size_x, dataset_size_y];

            // values
            for (int x = 0; x < dataset_size_x; x++)
            {
                for (int y = 0; y < dataset_size_y; y++)
                {
                    fake_dataset[x, y] = Convert.ToInt16(Math.Cos((double)x / 30) * Math.Cos((double)y / 30) * 500) + 500;
                }
            }

            dataset = fake_dataset;
        }

        public void EmptyDataset()
        {
            int[,] empty_dataset = new int[dataset_size_x, dataset_size_y];

            // values
            for (int x = 0; x < dataset_size_x; x++)
            {
                for (int y = 0; y < dataset_size_y; y++)
                {
                    //empty_dataset[x, y] = 0;
                }
            }

            //dataset = empty_dataset;
        }

        public void PrintDataset()
        {
            // values
            Console.Write('\n');
            for (int y = 0; y < dataset_size_y; y++)
            {
                for (int x = 0; x < dataset_size_x; x++)
                {
                    Console.Write(dataset[x, y].ToString() + "\t");
                }
                Console.Write('\n');
            }
        }

        public void setDataPoint(Int32 x, Int32 y, Int32 value)
        {
            dataset[x, y] = value;
            if (value > max_value)
                max_value = value;
            if (value < min_value)
                min_value = value;

        }

        private static Image createImg(int size_x, int size_y)
        {
            Bitmap img = new Bitmap(size_x, size_y);
            Graphics gimg = Graphics.FromImage(img);
            gimg.FillRectangle(Brushes.White, 0, 0, size_x, size_y);
            return img;
        }

        /// <summary>
        /// Vloží obrázek do obrázku pomocí pointerů na dané souřadnice
        /// </summary>
        /// <param name="A">Menší vkládaný obrázek</param>
        /// <param name="B">Podkladový obrázek</param>
        /// <param name="xO">Souřadnice x</param>
        /// <param name="yO">Souřadnice ÿ</param>
        /// <returns></returns>
        private bool putImageOnCoord(Bitmap A, Bitmap B, int xO, int yO)
        {
            // rozměry obrázků
            int photo_size_x = A.Width;
            int photo_size_y = A.Height;

            if (xO < -photo_size_x || xO > B.Width || yO < -photo_size_y || yO > B.Height)
            {
                return false;
            }

            BitmapData bmData = B.LockBits(new Rectangle(0, 0, B.Width, B.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            BitmapData bmDataPic = A.LockBits(new Rectangle(0, 0, A.Width, A.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            // spočteme souřadnice začátku fotku v pictureBoxu
            int startX = 0, startY = 0;
            int endX = 0, endY = 0;
            if (xO < 0)
                startX = 0;
            else
                startX = xO;

            if (yO < 0)
                startY = 0;
            else
                startY = yO;

            // kam až pojedeme
            if ((xO + photo_size_x) < B.Width)
                endX = xO + photo_size_x;
            else
                endX = B.Width;

            if ((yO + photo_size_y) < B.Height)
                endY = yO + photo_size_y;
            else
                endY = B.Height;

            // spočtem strajdy
            int stride = bmData.Stride;
            int stridePic = bmDataPic.Stride;

            // a pointery
            System.IntPtr Scan0 = bmData.Scan0;
            System.IntPtr Scan0Pic = bmDataPic.Scan0;

            unsafe
            {
                byte* p = (byte*)(void*)Scan0;
                byte* pPic = (byte*)(void*)Scan0Pic;

                // pocatecni podminky
                p += startX * 3 + stride * startY;
                if (xO < 0)
                    pPic += (photo_size_x - endX) * 3;
                if (yO < 0)
                    pPic += (photo_size_y - endY) * stridePic;

                // prirustky
                int nOffset = stride - ((endX - startX) * 3);
                int nOffsetPic = stridePic - ((endX - startX) * 3);

                for (int y = startY; y < endY; y++)
                {
                    for (int x = startX; x < endX; x++)
                    {
                        p[0] = pPic[0];
                        p[1] = pPic[1];
                        p[2] = pPic[2];

                        pPic += 3;
                        p += 3;
                    }
                    p += nOffset;
                    pPic += nOffsetPic;
                }
            }
            A.UnlockBits(bmDataPic);
            B.UnlockBits(bmData);


            return true;
        }

        public dimm getImgCoords(int size_X_px, int size_Y_px)
        {
            int dataset_size_scaled_x, dataset_size_scaled_y;
            // case with facing dataset to top and bottom edge
            if (((double)size_X_px / (double)dataset_size_x) > ((double)size_Y_px / (double)dataset_size_y))
            {
                dataset_size_scaled_x = Convert.ToInt32((double)dataset_size_x * ((double)size_Y_px / (double)dataset_size_y));
                dataset_size_scaled_y = size_Y_px;
            }
            // case with dataset faced to left and right edge 
            else
            {
                dataset_size_scaled_x = size_X_px;
                dataset_size_scaled_y = Convert.ToInt32((double)dataset_size_y * ((double)size_X_px / (double)dataset_size_x));
            }

            return new dimm(size_X_px / 2 - dataset_size_scaled_x / 2, size_X_px / 2 + dataset_size_scaled_x / 2,
                size_Y_px / 2 - dataset_size_scaled_y / 2, size_Y_px / 2 + dataset_size_scaled_y / 2);
        }
        
        internal Image getImage(int size_X_px, int size_Y_px)
        {
            // background image
            Bitmap img = (Bitmap)createImg(size_X_px, size_Y_px);

            //dataset image
            Bitmap dataimg = (Bitmap)createImg(dataset_size_x, dataset_size_y);

            BitmapData bmData = dataimg.LockBits(new Rectangle(0, 0, dataimg.Width, dataimg.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            int stride = bmData.Stride;
            System.IntPtr Scan0 = bmData.Scan0;

            unsafe
            {
                byte* p = (byte*)(void*)Scan0;

                int nOffset = stride - dataimg.Width * 3;
                int nWidth = dataimg.Width * 3;

                // tresholding
                for (int y = 0; y < dataimg.Height; y++)
                {
                    for (int x = 0; x < dataimg.Width; x++)
                    {
                        p[0] = p[1] = p[2] = ConvertData2color(dataset[x, y]);
                        p += 3;
                    }
                    p += nOffset;
                }
            }
            dataimg.UnlockBits(bmData);

            int dataset_size_scaled_x = dataset_size_x;
            int dataset_size_scaled_y = dataset_size_y;
            
            // case with facing dataset to top and bottom edge
            if (((double)size_X_px / (double)dataset_size_x) > ((double)size_Y_px / (double)dataset_size_y))
            {
                dataset_size_scaled_x = Convert.ToInt32((double)dataset_size_x * ((double)size_Y_px / (double)dataset_size_y));
                dataset_size_scaled_y = size_Y_px;
            }
            // case with dataset faced to left and right edge 
            else
            {
                dataset_size_scaled_x = size_X_px;
                dataset_size_scaled_y = Convert.ToInt32((double)dataset_size_y * ((double)size_X_px / (double)dataset_size_x));
            }

            // resize
            Bitmap resized = new Bitmap(dataimg, new Size(dataset_size_scaled_x, dataset_size_scaled_y));

            putImageOnCoord(resized, img, img.Width / 2 - dataset_size_scaled_x / 2, img.Height / 2 - dataset_size_scaled_y/2);

            return img;
        }

        public Image createScale(Size size)
        {
            // get minimum and maximum value
            Size minmax = getMinMax();
            double min = ConvertData2mm(minmax.Width);
            double max = ConvertData2mm(minmax.Height);

            // get increment
            double inc = (max - min) / 10;
            double dist = (double)size.Height / (double)10;

            Image img = createImg(size.Width, size.Height);
            LinearGradientBrush brush = new LinearGradientBrush(new Point(10, 0), new Point(10, size.Height-6), Color.White, Color.Black);
            Graphics g = Graphics.FromImage(img);
            g.FillRectangle(brush, size.Width / 2, 3, size.Width / 2, size.Height - 6);

            // draw scale (text)
            Font verdana = new Font("Verdana", 7);
            Font verdanaB = new Font("Verdana", 5);
            SolidBrush yellowBrush = new SolidBrush(Color.Red);
            SolidBrush blackBrush = new SolidBrush(Color.Black);

            for (int i = 0; i < 10; i++)
            {
                g.DrawString(Convert.ToString(Math.Round((min + (double)i * inc) * 10) / 10), verdanaB, blackBrush, 1, Convert.ToInt32(((double)i) * dist));
                //g.DrawString(Convert.ToString(Math.Round((min + i * inc) * 10) / 10), verdana, yellowBrush, 9, Convert.ToInt32(((double)i) * dist));
            }
            g.DrawString("[mm]", verdana, yellowBrush, 0, Convert.ToInt32(((double)9.5) * dist));

            return img;
        }

        public static double ConvertData2mm(int x)
        {
            double a0 = -6.748235905963436e-01;
            double a1 = 5.377627447295921e-02;
            double a2 = -1.402888941198118e-04;
            double a3 = 3.885917632292043e-07;

            return a0 + a1 * Convert.ToDouble(x) + a2 * Math.Pow(Convert.ToDouble(x), 2) + a3 * Math.Pow(Convert.ToDouble(x), 3);
        
        }
        public double ConvertData2mm(int x, int y)
        {
            return ConvertData2mm(dataset[x, y]);
        }

        public byte ConvertData2color(int value)
        {
            if (value == 0 || value == 1023)
                return 0;
            else if (min_value < max_value)
                return Convert.ToByte(255 * (double)(value - min_value) / (max_value - min_value));
            else
                return Convert.ToByte(255 * (double)value / 1024);
        }

        //returns a value of minimum and maximum
        public Size getMinMax()
        {
            int min = dataset[1,1];
            int max = dataset[1,1];

            for (int y = 0; y < dataset_size_y; y++)
            {
                for (int x = 0; x < dataset_size_x; x++)
                {
                    if (dataset[x, y] > max)
                        max = dataset[x, y];
                    if (dataset[x, y] < min)
                        min = dataset[x, y];
                }
            }

            return new Size(min, max);
        }
    }

    public struct dimm
    {
        public int x_min, x_max, y_min, y_max;

        public dimm(int xMin, int xMax, int yMin, int yMax)
        {
            x_min = xMin;
            x_max = xMax;
            y_min = yMin;
            y_max = yMax;
        }
    }
}
