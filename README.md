![makroskop_logo.png](https://bitbucket.org/repo/qoqaxk/images/1750803553-makroskop_logo.png)
# Model mikroskopu atomárních sil #

![2014-09-05 14.22.35.jpg](https://bitbucket.org/repo/qoqaxk/images/1588686443-2014-09-05%2014.22.35.jpg)

K běhu potřebuje ovladače k převodníku FT232RL, které je možné lehce stáhnout na webových stránkách podle použitého operačního systému.

Výukový model mikroskopu atomárních sil. Repozitář se skládá ze čtyř hlavních složek:


## Firmware ##

Obsahuje ovládací software pro mikrokontroler Atmel atmega168.

**Verze 1.0 (5.9.2014)**

Kompatibilní s PCB verze 1.0. Polohování funguje na principu otázka-odpověď pro každý bod zvlášť.

## PCB ##

** Verze 1.0 (18.8.2014)**
Zdrojové soubory k desce plošných spojů (Printed Circuit Board). Vytvořeny v Eagle 5.4.0 (otevřít v novějším není možné). Součástí je i složka se všemi potřebnými soubory pro průmyslovou výrobu PCB.
![pcb.png](https://bitbucket.org/repo/qoqaxk/images/1093246957-pcb.png)

## Software ##

Ovládací program napsaný v prostředí .NET 3.5 v jazyce C#.

**Verze 1.0 (5.9.2014)**

Kompatibilní s PCB a firmware verzemi 1.0. Dotazuje se bod po bodu. Pro prvotní lokalizaci je třeba na začátku najet na pravý přední bod.
![Screenshot 2014-09-04 18.07.07.png](https://bitbucket.org/repo/qoqaxk/images/2360026108-Screenshot%202014-09-04%2018.07.07.png)

## Obrázky ##

Složka s obrázky použitými v grafickém rozhraní ovládacího software. Obsahuje také schématický nákres z víka kufru, vytvořeném v programu Inkscape.